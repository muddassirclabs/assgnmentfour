package com.example.muddassiriqbal.modifiedcalculator.entities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.muddassiriqbal.modifiedcalculator.R;
import com.example.muddassiriqbal.modifiedcalculator.util.MainAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import static com.example.muddassiriqbal.modifiedcalculator.util.AppConstants.*;
import static com.example.muddassiriqbal.modifiedcalculator.MainActivity.historyMap;

public class SummaryActivity extends Activity {
    MainAdapter mainAdapter;
    LinkedList<String> history;
    ListView listView;
    Spinner dropDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        // Set Spinner(Drop Down Menu)
        dropDown = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, DROP_ITEMS);
        dropDown.setAdapter(arrayAdapter);

        listView = (ListView) findViewById(R.id.list_view);

        history = (LinkedList<String>) historyMap.get(ALL).clone();
        //Collections.reverse(history);
        mainAdapter = new MainAdapter(this, history);
        listView.setAdapter(mainAdapter);

        // Setting Item Select Listeners for Spinner(Drop Down)
        dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == FILTER_BY_ALL) {
                    showAll();

                } else if (position == FILTER_BY_ADDITION) {
                    filterByCondition(ADD);

                } else if (position == FILTER_BY_SUBTRACTION){
                    filterByCondition(SUBTRACT);

                } else if (position == FILTER_BY_MULTIPLICATION){
                    filterByCondition(MULTIPLY);

                } else if (position == FILTER_BY_DIVISION){
                    filterByCondition(DIVISION);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void filterByCondition(String condition) {
        history.clear();
        history = (LinkedList)historyMap.get(condition).clone();
        //Collections.reverse(history);
        mainAdapter = new MainAdapter(this, history);
        listView.setAdapter(mainAdapter);
        setResult(RESULT_OK);
    }

    private void showAll() {
        history = (LinkedList)historyMap.get(ALL).clone();
        //Collections.reverse(history);
        mainAdapter = new MainAdapter(this, history);
        listView.setAdapter(mainAdapter);
    }

    public void onClick(View view){
        historyMap.get(ALL).clear();
        historyMap.get(ADD).clear();
        historyMap.get(SUBTRACT).clear();
        historyMap.get(MULTIPLY).clear();
        historyMap.get(DIVISION).clear();
        history.clear();
        mainAdapter.notifyDataSetChanged();
    }
}
