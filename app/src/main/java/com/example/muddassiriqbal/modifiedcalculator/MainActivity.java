package com.example.muddassiriqbal.modifiedcalculator;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.muddassiriqbal.modifiedcalculator.entities.SummaryActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static com.example.muddassiriqbal.modifiedcalculator.util.AppConstants.*;


public class MainActivity extends Activity {
    double firstOperand, secondOperand, result;
    String operator;
    EditText firstOp, secondOp, resultant;
    Button addButton, subtractButton, multiplyButton, divideButton;
    LinkedList<String> historyAll = new LinkedList<>();
    LinkedList<String> historyAdd = new LinkedList<>();
    LinkedList<String> historySub = new LinkedList<>();
    LinkedList<String> historyMul = new LinkedList<>();
    LinkedList<String> historyDiv = new LinkedList<>();

    public static HashMap<String,LinkedList> historyMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstOp = (EditText) findViewById(R.id.first_operand);
        secondOp = (EditText) findViewById(R.id.second_operand);
        resultant = (EditText) findViewById(R.id.result);

        addButton = (Button)findViewById(R.id.add_button);
        subtractButton = (Button)findViewById(R.id.subtract_button);
        multiplyButton = (Button)findViewById(R.id.mul_button);
        divideButton = (Button)findViewById(R.id.div_button);

        historyMap.put(ALL,historyAll);
        historyMap.put(ADD,historyAdd);
        historyMap.put(SUBTRACT,historySub);
        historyMap.put(MULTIPLY,historyMul);
        historyMap.put(DIVISION,historyDiv);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_button:
                operator = ADD;
                doOperation();
                break;

            case R.id.subtract_button:
                operator = SUBTRACT;
                doOperation();
                break;

            case R.id.mul_button:
                operator = MULTIPLY;
                doOperation();
                break;

            case R.id.div_button:
                operator = DIVISION;
                doOperation();
                break;

            case R.id.summary_button:
                Intent intent = new Intent(this, SummaryActivity.class);
                startActivityForResult(intent, SHOW_SUMMARY);
                break;

            case R.id.clear:
                clearAll();
                break;
        }
    }

    private void clearAll() {

        // Reset Button Background
        addButton.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        subtractButton.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        multiplyButton.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        divideButton.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));

        // Reset Button Text Color
        divideButton.setTextColor(Color.parseColor(BLACK));
        multiplyButton.setTextColor(Color.parseColor(BLACK));
        subtractButton.setTextColor(Color.parseColor(BLACK));
        addButton.setTextColor(Color.parseColor(BLACK));

        // Reset EditText Fields
        firstOp.setText("");
        secondOp.setText("");
        resultant.setText("");

        // Set Focus to First Operand
        firstOp.requestFocus();

    }

    private void changeButtonColor(Button selectedButton, Button unselectedOne, Button unselectedTwo, Button unselectedThree) {
        selectedButton.setBackgroundColor(Color.parseColor(SELECTED_COLOR));
        selectedButton.setTextColor(Color.parseColor(WHITE));
        unselectedOne.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        unselectedTwo.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        unselectedThree.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        unselectedOne.setTextColor(Color.parseColor(BLACK));
        unselectedTwo.setTextColor(Color.parseColor(BLACK));
        unselectedThree.setTextColor(Color.parseColor(BLACK));
    }

    public void doOperation() {
        if (firstOp.getText().toString().equals("")) {
            Toast.makeText(this, INAPPROPRIATE_FIRST_OPERAND_MESSAGE, Toast.LENGTH_SHORT).show();

        } else if (secondOp.getText().toString().equals("")) {
            Toast.makeText(this, INAPPROPRIATE_SECOND_OPERAND_MESSAGE, Toast.LENGTH_SHORT).show();

        } else {
            firstOperand = Double.parseDouble(firstOp.getText().toString());
            secondOperand = Double.parseDouble(secondOp.getText().toString());

            if (operator.equals(ADD)) {
                result = firstOperand + secondOperand;
                resultant.setText("" + result);
                historyAll.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                historyAdd.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                changeButtonColor(addButton,subtractButton,multiplyButton,divideButton);
                Toast.makeText(this, historyAdd.get(historyAdd.size()-1), Toast.LENGTH_SHORT).show();

            } else if (operator.equals(SUBTRACT)) {
                result = firstOperand - secondOperand;
                resultant.setText("" + result);
                historyAll.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                historySub.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                changeButtonColor(subtractButton,addButton,multiplyButton,divideButton);
                Toast.makeText(this, historySub.get(historySub.size()-1), Toast.LENGTH_SHORT).show();

            } else if (operator.equals(MULTIPLY)) {
                result = firstOperand * secondOperand;
                resultant.setText("" + result);
                historyAll.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                historyMul.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                changeButtonColor(multiplyButton,addButton,subtractButton,divideButton);
                Toast.makeText(this, historyMul.get(historyMul.size()-1), Toast.LENGTH_SHORT).show();

            } else if (operator.equals(DIVISION)) {
                if(secondOperand == ZERO){
                    Toast.makeText(this, DIVIDE_BY_ZERO_ERROR, Toast.LENGTH_SHORT).show();

                } else {
                    result = firstOperand / secondOperand;
                    resultant.setText("" + result);
                    historyAll.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                    historyDiv.addFirst(firstOperand + operator + secondOperand + EQUAL_TO + result);
                    changeButtonColor(divideButton,addButton,subtractButton,multiplyButton);
                    Toast.makeText(this, historyDiv.get(historyDiv.size() - 1), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
