package com.example.muddassiriqbal.modifiedcalculator.util;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.muddassiriqbal.modifiedcalculator.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainAdapter extends BaseAdapter {
    LinkedList<String> data;
    Context context;

    public MainAdapter(Context ctx, LinkedList<String> data){
        this.data = data;
        this.context = ctx;
    }

    @Override
    public boolean isEnabled(int position){
        return false;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_main_adapter,null);
        TextView itemName = (TextView)view.findViewById(R.id.history_item);
        itemName.setText(data.get(position));
        return view;
    }
}