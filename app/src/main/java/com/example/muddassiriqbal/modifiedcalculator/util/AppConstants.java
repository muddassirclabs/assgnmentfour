package com.example.muddassiriqbal.modifiedcalculator.util;

public interface AppConstants {

    String[] DROP_ITEMS = {"All", "Add", "Subtract", "Multiply", "Divide"};

    String INAPPROPRIATE_FIRST_OPERAND_MESSAGE = "First Operand cannot be left blank!";
    String INAPPROPRIATE_SECOND_OPERAND_MESSAGE = "Second Operand cannot be left blank!";
    String DIVIDE_BY_ZERO_ERROR = "Cannot divide by Zero!";

    String HISTORY_KEY = "History";
    double ZERO = 0.0;

    // Operator
    String ADD = " + ";
    String SUBTRACT = " - ";
    String MULTIPLY = " x ";
    String DIVISION = " / ";
    String EQUAL_TO = " = ";
    String ALL = "All";

    // Color Constant
    String SELECTED_COLOR = "#009933";
    String UNSELECTED_COLOR = "#FFFFCC";
    String WHITE = "#FFFFFF";
    String BLACK = "#000000";

    // Fillter options
    int FILTER_BY_ALL = 0;
    int FILTER_BY_ADDITION = 1;
    int FILTER_BY_SUBTRACTION = 2;
    int FILTER_BY_MULTIPLICATION = 3;
    int FILTER_BY_DIVISION = 4;

    // Request Code
    int SHOW_SUMMARY = 10;



}
